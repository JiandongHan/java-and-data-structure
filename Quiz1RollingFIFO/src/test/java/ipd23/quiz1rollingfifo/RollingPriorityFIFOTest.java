/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ipd23.quiz1rollingfifo;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author James Han
 */
public class RollingPriorityFIFOTest {

    @Test
    public void testRollingPriorityFIFO() {
        RollingPriorityFIFO instance = new RollingPriorityFIFO(5);
        assertEquals(5, instance.sizeMax());
    }

    @Test
    public void testToArray() throws FIFOFullException {
        RollingPriorityFIFO instance = new RollingPriorityFIFO(5);
        assertEquals(5, instance.sizeMax());
        instance.enqueue("aaa", false);
        instance.enqueue("bbb", false);
        instance.enqueue("ccc", false);        
        assertEquals(3, instance.size());
        assertArrayEquals(new String[] {"aaa","bbb","ccc"}, instance.toArray());
    }

    @Test
    public void testNormalEnqueue() throws FIFOFullException {
        RollingPriorityFIFO instance = new RollingPriorityFIFO(5);
        assertEquals(5, instance.sizeMax());
        instance.enqueue("aaa", false);
        instance.enqueue("bbb", false);
        instance.enqueue("ccc", false);        
        assertEquals(3, instance.size());
        assertEquals("[aaa,bbb,ccc]", instance.toString());
    }
    
    @Test
    public void testOverEnqueue() throws FIFOFullException {
        Assertions.assertThrows(FIFOFullException.class, () -> {
        RollingPriorityFIFO instance = new RollingPriorityFIFO(5);
        instance.enqueue("aaa", false);
        instance.enqueue("bbb", false);
        instance.enqueue("ccc", false);   
        instance.enqueue("ddd", false);
        instance.enqueue("eee", false);   
        instance.enqueue("fff", false);
        });
    }    
    
    @Test
    public void testDequeueWithoutPriority()  throws FIFOFullException {
        RollingPriorityFIFO instance = new RollingPriorityFIFO(5);
        assertEquals(5, instance.sizeMax());
        instance.enqueue("aaa", false);
        instance.enqueue("bbb", false);
        instance.enqueue("ccc", false);        
        assertEquals(3, instance.size());
        assertEquals("[aaa,bbb,ccc]", instance.toString());
        
        instance.dequeue(false);        
        assertEquals(2, instance.size());
        assertEquals("[bbb,ccc]", instance.toString());
    }    
    
    @Test
    public void testDequeueWithPriority_1()  throws FIFOFullException {
        RollingPriorityFIFO instance = new RollingPriorityFIFO(5);
        assertEquals(5, instance.sizeMax());
        instance.enqueue("aaa", false);
        instance.enqueue("bbb", true);
        instance.enqueue("ccc", false);        
        assertEquals(3, instance.size());
        assertEquals("[aaa,bbb,ccc]", instance.toString());
        
        assertEquals("bbb",instance.dequeue(true));        
        assertEquals(2, instance.size());
        assertEquals("[aaa,ccc]", instance.toString());
    }    

    @Test
    public void testDequeueWithPriority_2()  throws FIFOFullException {
        RollingPriorityFIFO instance = new RollingPriorityFIFO(5);
        assertEquals(5, instance.sizeMax());
        instance.enqueue("aaa", true);
        instance.enqueue("bbb", false);
        instance.enqueue("ccc", false);        
        assertEquals(3, instance.size());
        assertEquals("[aaa,bbb,ccc]", instance.toString());
        
        assertEquals("aaa",instance.dequeue(true));        
        assertEquals(2, instance.size());
        assertEquals("[bbb,ccc]", instance.toString());
    }    
    
}
