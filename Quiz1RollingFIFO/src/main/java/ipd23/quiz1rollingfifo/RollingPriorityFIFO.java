/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ipd23.quiz1rollingfifo;


/**
 *
 * @author James Han
 */
public class RollingPriorityFIFO {
       private class Item {
      // add constructor of your choice
      Item next;
      boolean priority;
      String value;
    }

    private Item tail, head; // enqueue at tail, dequeue at head
    int itemsTotal, itemsCurrUsed;

    /* Parameter itemsTotal must be 5 or more, otherwise IllegalArgumentException
    * is thrown. Items are allocated and connected via next pointer only
    * once - here, in the constructor. After that they are re-used.
    */
    public RollingPriorityFIFO(int items) {
        itemsTotal = items;
        itemsCurrUsed = 0;
        Item previous = null;
        for (int i = 0; i < items; i++ ) {
            Item curItem = new Item();
            curItem.priority = false;
            curItem.value = null;
            if ( i == 0) {
                previous = curItem;
                head = curItem;
                tail = head;
            } else if ( i == (items - 1) ){
                previous.next = curItem;
                curItem.next = head;
            } else {
                previous.next = curItem;
                previous = curItem;
            }
        }
}

    // Places value in the next available Item. If FIFO is full throws exception.
    public void enqueue(String value, boolean priority) throws FIFOFullException {
        if (itemsCurrUsed >= itemsTotal) {
            throw new FIFOFullException();
        }
       
        if (itemsCurrUsed == 0) {
            head.priority = priority;
            head.value = value;
            tail = head;
            head = head.next;
            itemsCurrUsed++;
        }
        else {
            for (Item current = head; current != null; current = current.next) {
                if (current.value == null) {
                    current.priority = priority;
                    current.value = value;
                    head = current;
                    itemsCurrUsed++;
                    break;
                }
            }    
        }
    }

    /* returns null if fifo is empty, if it is not empty then
    * priority=true items are sarched first
    * if none is found then non-priority item is returned
    */

    public String dequeue(boolean priority) {
        String result = null;
        if (itemsCurrUsed == 0)
            return result;
        if ( !priority ) {
            result = tail.value;
            tail.value = null;
            tail = tail.next;
            itemsCurrUsed--;
            return result;
        } else {
            Item curItem = tail;
            
            if ( curItem.priority ) {
                result = curItem.value;
                curItem.value = null;
                tail = tail.next;
                itemsCurrUsed--;
                return result;
            }
            
            for (int i = 0; i < itemsCurrUsed-1; i++) {
                Item previous = curItem;
                curItem = curItem.next;
                if ( curItem.priority ) {
                    result = curItem.value;
                    previous.next = curItem.next;
                    curItem.value = null;
                    
                    curItem.next = head.next;
                    head.next = curItem;
                    break;
                }
            }
            itemsCurrUsed--;
        }
        
        return result;
    }

    public int size() {  // current FIFO size
        return itemsCurrUsed; 
    }
    
    public int sizeMax() {  // maximum FIFO size
        return itemsTotal; 
    }
    // Returns array of Strings of all items in FIFO (following next references).
    public String[] toArray() { 
        String[] result = new String[itemsCurrUsed];
        Item current = tail;
        for (int i = 0; i < itemsCurrUsed; i++) {
            if ( current.value != null ) {
                result[i] = current.value;
                current = current.next;
            }    
        }
        return result;
    }
   
    // Returns array of String only of priority items in FIFO.
    public String[] toArrayOnlyPriority() { 
        String[] result = new String[itemsCurrUsed];
        Item current = tail;
        for (int i = 0; i < itemsCurrUsed; i++) {
            if ( current.value != null && current.priority == true ) {
                result[i] = current.value;
                current = current.next;
            }    
        }
        return result;
    }

    // Items with priority=true have "*" appended, e.g. "[Jerry*,Maria,Tom*];
    // Items with priority=true have "*" appended, e.g. "[Jerry*,Maria,Tom*];
    // Items about to be dequeue()'d are listed first, items recently enqueue()'d last
    @Override
    public String toString() { 
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        // loop over all items in the linked list            
        Item current = tail;
        for ( int i = 0; i < itemsTotal; i++ ) {
            if ( current.value != null) {
                sb.append(i == 0 ? "" : ","); // no comma preceeding the first item
                sb.append(current.value);
            }    
            current = current.next;
        }
        sb.append("]");
        return sb.toString();
    }
}
