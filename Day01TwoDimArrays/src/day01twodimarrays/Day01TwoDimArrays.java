/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day01twodimarrays;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 *
 * @author James Han
 */
public class Day01TwoDimArrays {

    /**
     * @param args the command line arguments
     */
    static Scanner Input = new Scanner(System.in);
    
    public static void main(String[] args) {
        
        double standardDeviation = 0.0;
        // TODO code application logic here
            try {
            System.out.print("Please input the Array width and height: ");
            int width = Input.nextInt();
            if ( width < 1 ) {
               System.out.println("For the array width, you should input a interger greater than 0");
               return;
            } 
            int height = Input.nextInt();
            if ( height < 1 ) {
               System.out.println("For the array height, you should input a interger greater than 0");
               return;
            }            
            int intArray[][] = new int[width][height];
            
            for ( int i = 0; i < width; i++ ) {
                for ( int j = 0; j < height; j++ ) {
                    intArray[i][j] = (int) (Math.random()*99);     
                    System.out.printf("%s%d", j == 0 ? "" : ", ", intArray[i][j]);
                }
                System.out.println("");
            }

            System.out.println("");        

            int allSum = 0;
            for ( int i = 0; i < width; i++ ) {
                for ( int j = 0; j < height; j++ ) {
                    allSum += intArray[i][j];
                }
            }
            System.out.println("All number's sum is" + allSum);
            
            int rowSum = 0;
            for ( int i = 0; i < width; i++ ) {
                rowSum = 0;
                for ( int j = 0; j < height; j++ ) {
                    rowSum += intArray[i][j];
                }
                System.out.println("Rows " + i + " 's sum is : " + rowSum);
            }
            
            System.out.println("");

            int columnSum = 0;
            for ( int j = 0; j < height; j++ ) {
                columnSum = 0;
                for ( int i = 0; i < width; i++ ) {
                    columnSum += intArray[i][j];
                }
                System.out.println("Columns " + j + " 's sum is : " + columnSum);
            }
            
            System.out.println("");

            double mean = allSum/(width*height);
            
            for ( int i = 0; i < width; i++ ) {
                for ( int j = 0; j < height; j++ ) {
                    standardDeviation += (double)Math.pow(intArray[i][j] - mean, 2);
                }
            }
            System.out.printf("Standard Deviation = %.6f\n", Math.sqrt(standardDeviation/(width*height)));

            for ( int i = 0; i < width; i++ ) {
                for ( int j = 0; j < height; j++ ) {
                
                    for ( int m = i; m < width; m++ ) {
                        for ( int n = j; n < height; n++ ) { 
                            if ( m != i && n != j ) {
                                int num1 = intArray[i][j];
                                int num2 = intArray[m][n];
                                if ( isPrime(num1+num2)) {
                                    System.out.println("Pairs of ["+i+"]["+j+"] and ["+m+"]["+n+"] : sum is : "+(num1+num2));
                                }
                            }
                        }
                    }    

                }
            }
            
            System.out.println("");

            for ( int i = 0; i < width; i++ ) {
                for ( int j = 0; j < height; j++ ) {
                
                    for ( int m = 0; m < width; m++ ) {
                        for ( int n = 0; n < height; n++ ) { 
                            if ( m != i && n != j ) {
                                int num1 = intArray[i][j];
                                int num2 = intArray[m][n];
                                if ( isPrime(num1+num2)) {
                                    System.out.println("Pairs of ["+i+"]["+j+"] and ["+m+"]["+n+"] : sum is : "+(num1+num2));
                                }
                            }
                        }
                    }    

                }
            }
            
            
            /*
            for ( int i = 0; i < size; i++ ) {
                if ( isPrime( intArray[i] ) ) {
                    System.out.println(intArray[i] + " is a prime number");
                } 
            } 
            */
            
        }
        catch ( InputMismatchException ex ) {
            System.out.println("You should input a interger value: " + ex.getMessage());
        }
    }
    
    public static boolean isPrime(int number) {
        int i = 0,m = 0;
        
        m = number/2;      
        
        if( number == 0 || number == 1){  
            return false;      
        }else{  
            for(i = 2;i <= m; i++ ){      
                if( number % i == 0){      
                    return false;      
                }      
            }      
            return true; 
        }
    }   

}
