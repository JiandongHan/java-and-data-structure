/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ipd23.finaltreepatterns;
import java.util.*;  
/**
 *
 * @author James Han
 */


class Pair<K,V> {
    K key;
    V value;
    
    Pair(K key, V value) {
        this.key = key;
        this.value = value;
    }
    @Override
    public String toString() {
        return String.format("(%s=>%s)", key.toString(), value.toString());
    }
}

public class TreeStringIntSet implements Iterable<Pair<String,Integer>>  {
    
    class Node {
	Node left, right;
	String key; // keys are unique
	HashSet<Integer> valuesSet = new HashSet<>(); // unique only
	}

    @Override
    public Iterator<Pair<String,Integer>> iterator() {
        return new SimpleTreeIterator();
    }

    class SimpleTreeIterator implements Iterator<Pair<String,Integer>> {
        private int currIndex; 
        private List<Pair<String,Integer>> arrayOfPairs;

        SimpleTreeIterator() {
            arrayOfPairs = getAllKeyValPairs();
        }
        
        @Override
        public boolean hasNext() {
            return currIndex < arrayOfPairs.size();
        }

        @Override
        public Pair<String,Integer> next() {
            return arrayOfPairs.get(currIndex++);
        }
    }
    
    private Node root;
    private int nodesCount;

    // throws DuplicateValueException if this key already contains such value
    public void add(String key, int value) throws DuplicateValueException {
        Node node = new Node();
        node.key = key;
        node.left = null;
        node.right = null;
        
        Node current = root;
        if ( root == null ) {
            node.valuesSet.add(value);
            root = node;
            nodesCount++;
            return;
        } else {
            while ( current != null ) {
                if ( key.compareTo(current.key) > 0 ) {
                    if (current.right == null) { 
                        node.valuesSet.add(value);
                        current.right = node;
                        nodesCount++;
                        return;
                    }
                    else     
                        current = current.right;
                } else if ( current.key.compareTo(key) > 0 ) {
                    if (current.left == null) {  
                        node.valuesSet.add(value);
                        current.left = node;
                        nodesCount++;
                        return;
                    }    
                    else     
                        current = current.left;
                } else {
                    if (!current.valuesSet.contains(value)) {
                        current.valuesSet.add(value);
                        return;
                    } else 
                        throw new DuplicateValueException();
                } 
            }
        }
    }

    public int size() { 
        return nodesCount;
    } 
    
    public boolean containsKey(String key) { 
        Node current = root;
        
        if ( current.key.equals(key) ) {
            return true;
        } else {
            while ( current != null ) {
                if ( current.key.compareTo(key) > 0 ) {
                    current = current.left;
                } else if ( key.compareTo(current.key) > 0 ) {
                    current = current.right;
                } else { 
                    return true;
                }    
            }
        }
        return false;
    }

    public List<Integer> getValuesByKey(String key) {  // return empty list if key not found
        List<Integer> result = new ArrayList<Integer>();
        
        Node current = root;
        if ( current.key.equals(key) ) {
            for ( Integer i : current.valuesSet ) {
                System.out.println(i);
                result.add(i);
            }
            return result;
        } else {
            while ( current != null ) {
                if ( current.key.compareTo(key) > 0 ) {
                    current = current.left;
                } else if ( key.compareTo(current.key) > 0 ) {
                    current = current.right;
                } else { 
                    for ( Integer i : current.valuesSet ) {
                        result.add(i);
                    }
                    return result;                }    
                }
        }
        return null;
    }    
   
    public List<String> getKeysContainingValue(int value) { 
        collectKeysByValue(root, value);       
        return keysContainingValue;
    }
    
    private void collectKeysByValue(Node node, int value) {
        
        if (node == null)
            return;
        else {
            if (node.right != null);{
                collectKeysByValue(node.right,value);
                }
            
            if (node.valuesSet.contains(value)) {
                keysContainingValue.add(node.key);
            }
            
            if (node.left != null);
                collectKeysByValue(node.left,value);
        }
    }
    
    public List<String> getAllKeys() { 
        collectKeys(root);       
        return allKeys;
    } 

    private void collectKeys(Node node) {
        if (node == null)
            return;
        else {
            if (node.right != null);{
                collectKeys(node.right);
                }
            allKeys.add(node.key);
            if (node.left != null);
                collectKeys(node.left);
        }
    }    
    
    public List<Pair<String,Integer>> getAllKeyValPairs() { 
        resultArray = new ArrayList<>(nodesCount);
        resultIndex = 0;
        collectValuesInOrder(root);
        return resultArray;
    }
    
    private void collectValuesInOrder(Node node) {
        if (node == null)
            return;
        else {
            if (node.right != null)
                collectValuesInOrder(node.right);

            for ( Integer i : node.valuesSet) {
                Pair<String,Integer> p = new Pair<>(node.key,i);
                resultArray.add(p);
            }
            
            if (node.left != null);
                collectValuesInOrder(node.left);
        }
    }
    
    private List<Pair<String,Integer>> resultArray;
    List<String> keysContainingValue = new ArrayList<String>();
    List<String> allKeys = new ArrayList<String>();
    private int resultIndex;

    /*
    // add code for observer that is called on each node's two operations:
    // add succeeded or add failed.
    // It will take three parameters:
    // key, value being added, string describing operation ("Add" or "Add-Failed")
	
    public interface TreeEventObserverInt {
	public void event(String key, int value, String operation);
    }
	
    //Pair<String,Integer> iterator() {  // needed for iterator
    //    return null;
    //}
    // You can add toString() for debugging if you like
	
    // You can add other private methods and fields as needed. But no public ones.
    */
    
    public static void main(String[] args) throws DuplicateValueException {
        // TODO code application logic here
        
        TreeStringIntSet instance = new TreeStringIntSet();
        instance.add("fff",5);
        instance.add("aaa",6);
        instance.add("kkk",8);
        instance.add("fff",8);
        
        List<String> re = instance.getKeysContainingValue(8);
        for (String i : re ) {
            System.out.println(i);    
        }
        /*
        List<String> res = instance.getAllKeys();
        for (String i : res ) {
            System.out.println(i);    
        }
        
        instance.getAllKeyValPairs();
        
        for (Pair<String,Integer> pair : instance) { 
            System.out.printf("%s => %d\n", pair.key, pair.value);
        }
        
        instance.getKeysContainingValue(8);
        System.out.println(instance.getKeysContainingValue(8).toString());
        */
    }
}
