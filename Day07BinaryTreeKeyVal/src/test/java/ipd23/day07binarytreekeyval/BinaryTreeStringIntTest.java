/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ipd23.day07binarytreekeyval;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author James Han
 */
public class BinaryTreeStringIntTest {
    
    @Test
    public void testBinaryTreeStringInt_1() {
        BinaryTreeStringInt instance = new BinaryTreeStringInt();
        instance.put("fff",5);
        instance.put("bbb",3);
        instance.put("aaa",7);
        instance.put("kkk",1);
        instance.put("jjj",9);
        instance.put("sss",11);        
        assertEquals(6, instance.size());
    }
    
    @Test
    public void testBinaryTreeStringInt_2() {
        BinaryTreeStringInt instance = new BinaryTreeStringInt();
        instance.put("fff",5);
        instance.put("bbb",3);
        instance.put("aaa",7);
        instance.put("kkk",1);
        instance.put("jjj",9);
        instance.put("sss",11);        
        assertEquals(6, instance.size());
        assertEquals(5, instance.getValByKey("fff"));
        assertEquals(11, instance.getValByKey("sss"));        
        
        
    }
}
