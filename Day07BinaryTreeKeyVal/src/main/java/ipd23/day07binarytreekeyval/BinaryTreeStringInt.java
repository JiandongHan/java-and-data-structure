/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ipd23.day07binarytreekeyval;

/**
 *
 * @author James Han
 */
public class BinaryTreeStringInt {
    private class Node {
        String key;
        int value;
        Node left, right;
    }

    Node root;
    private int nodesCount;
	
    // throws exception if put attempts to insert a key that already exists (a duplicate)
    // values may be duplicates but keys may not
    public void put(String key, int value) throws IllegalArgumentException {
        Node node = new Node();
        node.key = key;
        node.value = value;
        node.left = null;
        node.right = null;
        
        Node current = root;
        if ( root == null ) {
            root = node;
            nodesCount++;
            return;
        } else {
            while ( current != null ) {
                if ( current.key.compareTo(key) > 0 ) {
                    if (current.left == null) { 
                        current.left = node;
                        nodesCount++;
                        return;
                    }
                    else     
                        current = current.left;
                } else if ( key.compareTo(current.key) > 0 ) {
                    if (current.right == null) {  
                        current.right = node;
                        nodesCount++;
                        return;
                    }    
                    else     
                        current = current.right;
                } else 
                    throw new IllegalArgumentException();
            }
        }
        
    }
	
    // print out all key-value pairs (one per line) from the smallest key to the largest
    public void printAllKeyValPairs() { 
        collectValuesInOrder(root);
    }

    public int size() { 
        return nodesCount;
    }    
    
    public int getValByKey(String key) {
        Node current = root;
        if ( current.key.equals(key) ) {
            return current.value;
        } else {
            while ( current != null ) {
                if ( current.key.compareTo(key) > 0 ) {
                    current = current.left;
                } else if ( key.compareTo(current.key) > 0 ) {
                    current = current.right;
                } else { 
                    return current.value;
                }    
            }
        }
        return -1;
    }
    
    private void collectValuesInOrder(Node node) {
        if (node == null)
            return;
        else {
            if (node.right != null);{
                collectValuesInOrder(node.right);
                }
            
            System.out.println("[Node key : "+ node.key + " ; Node value : " + node.value);
            
            if (node.left != null);
                collectValuesInOrder(node.left);

        }
    }
    
    public static void main(String[] args) {
        // TODO code application logic here
        
        BinaryTreeStringInt instance = new BinaryTreeStringInt();
        instance.put("fff",5);
        instance.put("bbb",3);
        instance.put("aaa",7);
        instance.put("kkk",1);
        instance.put("jjj",9);
        instance.put("sss",11);        

        instance.printAllKeyValPairs();
    }
    
}
