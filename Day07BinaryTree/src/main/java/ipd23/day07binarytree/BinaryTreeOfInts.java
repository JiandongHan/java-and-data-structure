/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ipd23.day07binarytree;

/**
 *
 * @author James Han
 */
public class BinaryTreeOfInts {
    private class NodeOfInt {
        int value; // could also be key,value pair
        NodeOfInt left, right;
    }
    
    NodeOfInt root;
    private int nodesCount;
    private int[] resultArray;
    private int resultIndex;    
    
    // throws exception if put attempts to insert value that already exists (a duplicate)
    public void put(int value) throws IllegalArgumentException { 
        NodeOfInt ni = new NodeOfInt();
        ni.value = value;
        ni.left = null;
        ni.right = null;
        
        NodeOfInt current = root;
        if ( root == null ) {
            root = ni;
            nodesCount++;
            return;
        } else {
            while ( current != null ) {
                if ( value < current.value ) {
                    if (current.left == null) { 
                        current.left = ni;
                        nodesCount++;
                        return;
                    }
                    else     
                        current = current.left;
                } else if ( value > current.value ) {
                    if (current.right == null) {  
                        current.right = ni;
                        nodesCount++;
                        return;
                    }    
                    else     
                        current = current.right;
                } else 
                    throw new IllegalArgumentException();
            }
        }
    }

    // uses recursion to compute the sum of all values in the entire tree
    public int getSumOfAllValues() { 
        int sum = 0;
        sum = getSumOfThisAndSubNodes(root);
        return sum;
    }
    // private helper recursive method to implement the above method
    private int getSumOfThisAndSubNodes(NodeOfInt node) { 
        int sum = 0;
        if (node == null)
            return 0;
        else {
            sum = node.value + getSumOfThisAndSubNodes(node.left) + getSumOfThisAndSubNodes(node.right);
        }
        return sum;
    }
    
    public int size() {  // current FIFO size
        return nodesCount; 
    }
    	
    // uses recursion to collect all values from largest to smallest
    public int [] getValuesInOrder() { // from largest to smallest
        resultArray = new int[nodesCount];
        resultIndex = 0;
        collectValuesInOrder(root);
        return resultArray;
    }

    // private helper recursive method to implement the above method
    private void collectValuesInOrder(NodeOfInt node) {
         
        
        if (node == null)
            return;
        else {
            if (node.right != null);{
                collectValuesInOrder(node.right);
                }
            
            resultArray[resultIndex] = node.value;
            System.out.println("Node value is: "+ resultArray[resultIndex] + " , resultIndex is : " + resultIndex);
            
            resultIndex++;

            if (node.left != null);
                collectValuesInOrder(node.left);

        }
    }
    
    @Override
    public String toString() { 
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        // loop over all items in the linked list            
        for ( int i = 0; i < resultArray.length; i++ ) {
            sb.append(i == 0 ? "" : ","); // no comma preceeding the first item
            sb.append(resultArray[i]+"");
        }
        sb.append("]");
        return sb.toString();
    }
    
    // data structures used to make collecting values in order easier
    
    public static void main(String[] args) {
        // TODO code application logic here
        
        BinaryTreeOfInts instance = new BinaryTreeOfInts();
        instance.put(5);
        instance.put(3);
        instance.put(1);
        instance.put(7); 
        
        int[] result = new int[instance.nodesCount];
        result = instance.getValuesInOrder();
        for (int i = 0; i<result.length; i++)
            System.out.println(result[i]);
    }
}
