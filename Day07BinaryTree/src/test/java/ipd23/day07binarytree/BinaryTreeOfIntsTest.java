/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ipd23.day07binarytree;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author James Han
 */
public class BinaryTreeOfIntsTest {
    
    @Test
    public void testBinaryTreePut() {
        BinaryTreeOfInts instance = new BinaryTreeOfInts();
        instance.put(5);
        instance.put(3);
        instance.put(1);
        instance.put(7);        
        assertEquals(4, instance.size());
        assertEquals(16, instance.getSumOfAllValues());
        instance.getValuesInOrder();
        assertEquals("[7,5,3,1]", instance.toString());
    }
   
    @Test
    public void testtestBinaryTreePutWithDuplicatedValue() throws IllegalArgumentException {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
        BinaryTreeOfInts instance = new BinaryTreeOfInts();
        instance.put(5);
        instance.put(3);
        instance.put(1);
        instance.put(7);        
        instance.put(3);                
        });
    }  

    @Test
    public void testBinaryTreePutMix() {
        BinaryTreeOfInts instance = new BinaryTreeOfInts();
        instance.put(5);
        instance.put(3);
        instance.put(1);
        instance.put(7);        
        instance.put(8);
        instance.put(11);
        instance.put(35);
        instance.put(22);                
        assertEquals(8, instance.size());
        assertEquals(92, instance.getSumOfAllValues());
        assertArrayEquals(new int[] {35,22,11,8,7,5,3,1}, instance.getValuesInOrder());
    }    
}
