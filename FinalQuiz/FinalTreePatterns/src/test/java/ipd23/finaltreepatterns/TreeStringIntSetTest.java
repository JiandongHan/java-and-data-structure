/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ipd23.finaltreepatterns;

import org.junit.jupiter.api.Assertions;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

/**
 *
 * @author James Han
 */
public class TreeStringIntSetTest {
    
    @Test
    // Different key added
    public void testTreeStringIntSet_Add1() throws DuplicateValueException {
        TreeStringIntSet instance = new TreeStringIntSet();
        instance.add("fff",5);
        instance.add("aaa",8);
        instance.add("ccc",7);
        instance.add("kkk",15);
        assertEquals(4, instance.size());
    }

    // Same key with different value. the nodes count doesn't change
    public void testTreeStringIntSet_Add2() throws DuplicateValueException {
        TreeStringIntSet instance = new TreeStringIntSet();
        instance.add("fff",5);
        instance.add("aaa",8);
        instance.add("fff",7);
        instance.add("kkk",15);
        assertEquals(3, instance.size());
    }
    
    
    @Test
    public void testTreeStringIntSet_3() throws DuplicateValueException {
        Assertions.assertThrows(DuplicateValueException.class, () -> {
        TreeStringIntSet instance = new TreeStringIntSet();
        instance.add("fff",5);
        instance.add("aaa",8);
        instance.add("fff",5); // the same key with same value, Excption should be throwed
        instance.add("kkk",15);
        });
    }
    
    // Test the containsKey
    @Test
    public void testTreeStringIntSet_containsKey() throws DuplicateValueException {
        TreeStringIntSet instance = new TreeStringIntSet();
        instance.add("fff",5);
        instance.add("aaa",8);
        instance.add("fff",7);
        instance.add("kkk",15);
        assertEquals(true, instance.containsKey("fff"));
        assertEquals(true, instance.containsKey("kkk"));
        assertEquals(false, instance.containsKey("mmm"));
    }
    
    // Test the getValuesByKey
    @Test
    public void testTreeStringIntSet_getValuesByKey_1() throws DuplicateValueException {
        TreeStringIntSet instance = new TreeStringIntSet();
        instance.add("fff",5);
        instance.add("aaa",8);
        instance.add("fff",7);
        instance.add("kkk",15);
        assertEquals(5, instance.getValuesByKey("fff").get(0));
        assertEquals(7, instance.getValuesByKey("fff").get(1));
    }
    
    // Test the getValuesByKey
    @Test
    public void testTreeStringIntSet_getValuesByKey_2() throws DuplicateValueException {
        TreeStringIntSet instance = new TreeStringIntSet();
        instance.add("fff",5);
        instance.add("aaa",8);
        instance.add("fff",7);
        instance.add("kkk",15);
        assertEquals(5, instance.getValuesByKey("fff").get(0));
        assertEquals(7, instance.getValuesByKey("fff").get(1));
        assertEquals(8, instance.getValuesByKey("aaa").get(0));
    }

    // Test the getKeysContainingValue    
    @Test 
    public void testTreeStringIntSet_getKeysContainingValue() throws DuplicateValueException {
        TreeStringIntSet instance = new TreeStringIntSet();
        instance.add("fff",5);
        instance.add("aaa",8);
        instance.add("fff",7);
        instance.add("kkk",8);
        assertEquals("kkk", instance.getKeysContainingValue(8).get(0));
        assertEquals("aaa", instance.getKeysContainingValue(8).get(1));
        //assertArrayEquals(new String[] {"kkk","aaa"},instance.getKeysContainingValue(8).toArray());
    }

    // Test the getAllKeys    
    @Test 
    public void testTreeStringIntSet_getAllKeys() throws DuplicateValueException {
        TreeStringIntSet instance = new TreeStringIntSet();
        instance.add("fff",5);
        instance.add("aaa",8);
        instance.add("fff",7);
        instance.add("kkk",8);
        assertEquals("kkk", instance.getAllKeys().get(0));
        assertEquals("fff", instance.getAllKeys().get(1));
        assertEquals("aaa", instance.getAllKeys().get(2));
    }
    
    // Test the getAllKeyValPairs()    
    @Test 
    public void testTreeStringIntSet_getAllKeyValPairs() throws DuplicateValueException {
        TreeStringIntSet instance = new TreeStringIntSet();
        instance.add("fff",5);
        instance.add("aaa",8);
        instance.add("fff",7);
        instance.add("kkk",8);
        assertEquals("kkk", instance.getAllKeyValPairs().get(0).key);
        assertEquals(8, instance.getAllKeyValPairs().get(0).value);
        assertEquals("fff", instance.getAllKeyValPairs().get(1).key);
        assertEquals(5, instance.getAllKeyValPairs().get(1).value);
    }
    
}
