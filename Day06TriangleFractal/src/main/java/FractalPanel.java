
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Polygon;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author James Han
 */
public class FractalPanel extends javax.swing.JPanel {

    /**
     * Creates new form FractalPanel
     */
    int xPoly[] = {400, 780, 20};
    int yPoly[] = {71, 729, 729};
    Polygon poly = new Polygon(xPoly, yPoly, xPoly.length);
    
    public FractalPanel() {
        initComponents();
    }
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g.create();
        //g2d.drawLine(10, getHeight()/10, getWidth()-10, getHeight()/10);
        g2d.setColor(Color.BLUE);
        g2d.drawPolygon(poly);
        drawFractal(g2d, xPoly, yPoly, 5);
    }

    private void drawFractal(Graphics2D g2d, int[] xpoly, int[] ypoly, int times) {
        // 1. condition when the recursion ends
        if ( times == 0 )
            return;
        int newPolyX[] = new int[3];
        int newPolyY[] = new int[3];

        newPolyX[0] = (xpoly[0] + xpoly[2])/2;
        newPolyY[0] = (ypoly[0] + ypoly[2])/2;

        newPolyX[1] = (xpoly[0] + xpoly[1])/2;
        newPolyY[1] = (ypoly[0] + ypoly[1])/2;        
      
        newPolyX[2] = (xpoly[2] + xpoly[1])/2;
        newPolyY[2] = (ypoly[2] + ypoly[1])/2;
        
        Polygon newPoly = new Polygon(newPolyX, newPolyY, newPolyY.length);
        g2d.drawPolygon(newPoly);

        int polyX0[] = new int[3];
        int polyY0[] = new int[3];        
        
        polyX0[0] = xpoly[2];
        polyY0[0] = ypoly[2];
        polyX0[1] = newPolyX[0];
        polyY0[1] = newPolyY[0];
        polyX0[2] = newPolyX[2];
        polyY0[2] = newPolyY[2];        
        drawFractal(g2d, polyX0, polyY0, times-1);

        
        int polyX1[] = new int[3];
        int polyY1[] = new int[3];        
        
        polyX1[0] = xpoly[0];
        polyY1[0] = ypoly[0];
        polyX1[1] = newPolyX[0];
        polyY1[1] = newPolyY[0];
        polyX1[2] = newPolyX[1];
        polyY1[2] = newPolyY[1];        
        drawFractal(g2d, polyX1, polyY1, times-1);        

        int polyX2[] = new int[3];
        int polyY2[] = new int[3];        
        
        polyX2[0] = xpoly[1];
        polyY2[0] = ypoly[1];
        polyX2[1] = newPolyX[2];
        polyY2[1] = newPolyY[2];
        polyX2[2] = newPolyX[1];
        polyY2[2] = newPolyY[1];        
        drawFractal(g2d, polyX2, polyY2, times-1);        
        
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 693, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
