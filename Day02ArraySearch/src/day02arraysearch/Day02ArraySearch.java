/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day02arraysearch;

/**
 *
 * @author James Han
 */
public class Day02ArraySearch {

    /**
     * @param args the command line arguments
     */
    static int getAboveIfExists(int[][] data, int row, int col) {
	// If exists, return the element, otherwise return 0
        if ( row < 0 || row >= data.length || col < 0 || col >= data[row].length ) {
            return 0;
        } 
            
        if ( row == 0 )
            return 0;
        else 
            return data[row-1][col];
    }

    static int getBelowIfExists(int[][] data, int row, int col) {
	// If exists, return the element, otherwise return 0
        if ( row < 0 || row >= data.length || col < 0 || col >= data[row].length ) {
            return 0;
        } 
        
        if ( row == data.length - 1 )
            return 0;
        else 
            return data[row+1][col];
    }
    
    static int getLeftIfExists(int[][] data, int row, int col) {
	// If exists, return the element, otherwise return 0
        if ( row < 0 || row >= data.length || col < 0 || col >= data[row].length ) {
            return 0;
        } 
                
        if ( col == 0 )
            return 0;
        else 
            return data[row][col-1];
    }

    static int getRightIfExists(int[][] data, int row, int col) {
	// If exists, return the element, otherwise return 0
        if ( row < 0 || row >= data.length || col < 0 || col >= data[row].length ) {
            return 0;
        }
        
        if ( col == data[row].length - 1 )
            return 0;
        else 
            return data[row][col+1];
    }
    
    static int sumOfCross(int[][] data, int row, int col) {
	// return sum of the element at row/col
        // plus (if they exist) element above, below, to the left and right of it
        if ( row < 0 || row >= data.length || col < 0 || col >= data[row].length ) {
            System.out.println("Wrong index of array's");
            return 0;
        } 
        
        return data[row][col] 
                + getAboveIfExists(data, row, col)
                + getBelowIfExists(data, row, col)
                + getLeftIfExists(data, row, col)
                + getRightIfExists(data, row, col);
    }
    
	// useful helper for debuggin
    static void print2D(int[][] data) { 
        System.out.println("Array data is : ");
        for ( int row = 0; row < data.length; row++ ) {
            for ( int col = 0; col < data[row].length; col++ ) {
                System.out.printf("%s%d", col == 0 ? "" : ", ", data[row][col]);
            }
            System.out.println("");
        }
        System.out.println("");
    }
	
    static int[][] createNewArray(int[][] data) {
        int[][] newArray = new int[data.length][data[0].length];
        
        for ( int row = 0; row < data.length; row++ ) {
            for ( int col = 0; col < data[row].length; col++ ) {
                newArray[row][col] = sumOfCross(data, row, col);
            }
        }
        
        return newArray;
    }
    
    public static void main(String[] args) {
        // TODO code application logic here
        int[][] data2D = {
            {1, 3, 6, 8},
            {7, 1, 2, 3},
            {8, 3, 2, 1},
            {1, 7, 1, 9},
        };
    
        int row = 1;
        int col = 3;
        int above = getAboveIfExists(data2D, row, col);
        int below = getBelowIfExists(data2D, row, col);
        int left = getLeftIfExists(data2D, row, col);
        int right = getRightIfExists(data2D, row, col);
        System.out.printf("above is : %d, below is : %d, left is : %d, right is : %d. \n", above, below, left, right);
        System.out.printf("sumOfCross is : %d\n", sumOfCross(data2D, row, col));
    
        int value = sumOfCross(data2D, 0, 0);
        int rows = 0, cols = 0;
        for ( int i = 0; i < data2D.length; i++ ) {
            for ( int j = 0; j< data2D[i].length; j++ ) {
                if ( sumOfCross(data2D, i, j) < value ) {
                   value = sumOfCross(data2D, i, j);
                   rows = i;
                   cols = j;
                }
            }
        }
        System.out.printf("Smallest sumOfCross is : %d, row is : %d, col is : %d. \n", sumOfCross(data2D, rows, cols), rows, cols);
    
        int[][] newArrays = createNewArray(data2D);
        print2D(newArrays);
    }
    
}
