/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ipd23.day07binarytreekeyvalgeneric;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author James Han
 * @param <K>
 * @param <V>
 */
class Pair<K,V> {
    public K key;
    public V value;
}

/*
class ArrayPair<T>  {
    public Object[] obj_array;   //object array
    public int length;
    // class constructor
    public ArrayPair(int length)    {
        // instantiate a new Object array of specified length
        obj_array = new Object [length];
        this.length = length;
    }
    // get obj_array[i]
    T get(int i) {
        @SuppressWarnings("unchecked")
        T e = (T)obj_array[i];
        return e;
    }
    // set e at obj_array[i]
    void set(int i, T e) {
        obj_array[i] = e;
    }
}
*/
public class BinaryTree<K extends Comparable <K> ,V extends Comparable <V>> implements Iterable< Pair<K,V>> {

    @Override
    public Iterator<Pair<K, V>> iterator() {
        return new SimpleBinaryTreeIterator();
    }

    class SimpleBinaryTreeIterator implements Iterator<Pair<K, V>> {
        private int currIndex; 
        private List<Pair<K,V>> arrayOfPairs;

        SimpleBinaryTreeIterator() {
            arrayOfPairs = printAllKeyValPairs();
        }
        
        @Override
        public boolean hasNext() {
            return currIndex < arrayOfPairs.size();
        }

        @Override
        public Pair<K,V> next() {
            return arrayOfPairs.get(currIndex++);
        }
    }
    
    private class Node {
        K key;
        V value;
        Node left, right;
    }

    Node root;
    private int nodesCount;
	
    // throws exception if put attempts to insert a key that already exists (a duplicate)
    // values may be duplicates but keys may not
    public void put(K key, V value) throws IllegalArgumentException {
        Node node = new Node();
        node.key = key;
        node.value = value;
        node.left = null;
        node.right = null;
        
        Node current = root;
        if ( root == null ) {
            root = node;
            nodesCount++;
            return;
        } else {
            while ( current != null ) {
                //if ( value < current.value ) {
                if ( value.compareTo(current.value) < 0 ) {
                    if (current.left == null) { 
                        current.left = node;
                        nodesCount++;
                        return;
                    }
                    else     
                        current = current.left;
                } else if ( value.compareTo(current.value) > 0 ) {
                    if (current.right == null) {  
                        current.right = node;
                        nodesCount++;
                        return;
                    }    
                    else     
                        current = current.right;
                } else 
                    throw new IllegalArgumentException();
            }
        }
    }
   
    // print out all key-value pairs (one per line) from the smallest key to the largest
    public List<Pair<K,V>> printAllKeyValPairs() { 
        resultArray = new ArrayList<>(nodesCount);
        resultIndex = 0;
        collectValuesInOrder(root);
        return resultArray;
    }
    
    private void collectValuesInOrder(Node node) {
        if (node == null)
            return;
        else {
            if (node.right != null)
                collectValuesInOrder(node.right);

            
            Pair<K,V> p = new Pair<>();
            p.key = node.key;
            p.value = node.value;
            resultArray.add(p);
            
            resultIndex++;
            
            System.out.printf("Key is %d  Value is %s.\n",node.key ,node.value);
            
            if (node.left != null);
                collectValuesInOrder(node.left);
        }
    }
    
    private List<Pair<K,V>> resultArray;
    private int resultIndex;
    
    public static void main(String[] args) {
        // TODO code application logic here
        
        BinaryTree<Integer,String> instance = new BinaryTree<Integer,String>();
        instance.put(1,"aaa");
        instance.put(2,"bbb");
        instance.put(3,"ccc");
        instance.put(4,"ddd");
        instance.put(5,"eee");
        instance.put(6,"fff");        

        instance.printAllKeyValPairs();
        
        for (Pair<Integer,String> pair : instance) { 
            System.out.printf("%d => %s\n", pair.key, pair.value);
        }
        
    }
    
}
