/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ipd23.day01arrays;
import java.util.*;
import java.io.*;
import java.util.Random;

/**
 *
 * @author James Han
 */
class InvalidDataException extends Exception {

    InvalidDataException(String msg) {
        super(msg);
    }

    InvalidDataException(String msg, Throwable orig) {
        super(msg, orig);
    }
}

public class ArraysOperation {
        /**
     * @param args the command line arguments
     */
    static Scanner Input = new Scanner(System.in);
    

    
    public static void main(String args[]) {

        try {
            System.out.println("Please input the Array Size");
            int size = Input.nextInt();
            if ( size < 1 ) {
               System.out.println("You should input a interger greater than 0");
               return;
            } 
            
            int intArray[] = new int[size];
            
            for ( int i = 0; i < size; i++ ) {
                intArray[i] = (int) (Math.random()*99 + 1);     
                System.out.printf("%s%d", i == 0 ? "" : ",", intArray[i]);
            }

            System.out.println("");        

            for ( int i = 0; i < size; i++ ) {
                if ( isPrime( intArray[i] ) ) {
                    System.out.println(intArray[i] + " is a prime number");
                } 
            }            
        }
        catch ( InputMismatchException ex ) {
            System.out.println("You should input a interger value: " + ex.getMessage());
        }

    }
    
    private static boolean isPrime(int number) {
        int i = 0,m = 0;
        
        m = number/2;      
        
        if( number == 0 || number == 1){  
            return false;      
        }else{  
            for(i = 2;i <= m; i++ ){      
                if( number % i == 0){      
                    return false;      
                }      
            }      
            return true; 
        }
    }
}
