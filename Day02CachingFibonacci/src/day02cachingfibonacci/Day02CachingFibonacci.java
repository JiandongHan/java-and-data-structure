/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day02cachingfibonacci;

import java.util.ArrayList;

/**
 *
 * @author James Han
 */
public class Day02CachingFibonacci {

    /**
     * @param args the command line arguments
     */
   
    static class FibCached {

	public FibCached() {
            fibsCached = new ArrayList<>();
            fibsCached.add((long)0); // #0
            fibsCached.add((long)1); // #1
            fibsCompCount = 2;
	}

	private ArrayList<Long> fibsCached; 
	private int fibsCompCount;
        // in a correct caching implementation fibsCompCount will end up the same as fibsCached.size();

        public long getNthFib(int n) {
            if ( n < 0 ) {
                return -1;
            } else if ( n < fibsCompCount ) {
                return fibsCached.get(n);
            } else 
                return computeNthFib(n);
        }
	
	// You can find implementation online, recursive or non-recursive.
	// For 100% solution you should use values in fibsCached as a starting point
	// instead of always starting from the first two values of 0, 1.
	private long computeNthFib(int n) {
            long f_n_minus_1;
            long f_n_minus_2;
            
            int i = fibsCompCount;
            while ( i <= n ) {
                f_n_minus_1 = getNthFib(i-1);
                f_n_minus_2 = getNthFib(i-2);
                fibsCached.add( f_n_minus_1 + f_n_minus_2 );
                fibsCompCount++;
                i++;
            }
            return getNthFib(fibsCompCount-1);
        }
	
	// You are allowed to add another private method for fibonacci generation
	// if you want to use recursive approach. I recommend non-recursive though.

	// How many fibonacci numbers has your code computed as opposed to returned cached?
	// Use this in your testing to make sure your caching actually works properly.
	public int getCountOfFibsComputed() {
            return fibsCompCount;
        }

	@Override
	public String toString() { // returns all cached Fib values, comma-space-separated on a single line
            String result = "";
            
            for (int i = 0; i<fibsCached.size(); i++ ) {
                result += String.format("%d : %d ,", i, fibsCached.get(i));
                
            }
            return result;
        }
    }    
    
    public static void main(String[] args) {
        // TODO code application logic here
        FibCached fcache = new FibCached(); 
        
        System.out.println(fcache.toString());
        fcache.getNthFib(2);
        System.out.println(fcache.toString());
        fcache.getNthFib(3);
        System.out.println(fcache.toString());
        fcache.getNthFib(10);
        System.out.println(fcache.toString());
        System.out.printf("fibsCompCount is %d now.",fcache.fibsCompCount);
    }
    
}
