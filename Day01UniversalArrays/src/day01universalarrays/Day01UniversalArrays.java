/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day01universalarrays;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 *
 * @author James Han
 */
public class Day01UniversalArrays {

    /**
     * @param args the command line arguments
     */
    static Scanner Input = new Scanner(System.in);
    static int data2d[][];
    
    public static int[] inputArray() {
        int[] data = null;
        try {
            System.out.print("Please input the array's length: ");
            int length = Input.nextInt();
            if ( length < 0 ) {
               System.out.println("The array length should be an interger greater than 0");
               return null;
            } 
            data = new int[length];
            for ( int i = 0; i < length; i++ ) {
                System.out.printf("Enter value of array[%d] : ", i);
                data[i] = Input.nextInt();
            }
        }
        catch ( InputMismatchException ex ) {
            System.out.println("You should input a interger value: " + ex.getMessage());
        }
        return data;
    }
    
    static void printArray(int[] data) {
        try {
            if ( data.length == 0 ) {
                System.out.println("data.length == 0");
            } else {
                System.out.println("Array data is : ");
                for ( int i = 0; i < data.length; i++ ) {
                    System.out.printf("%s%d", i == 0 ? "" : ", ", data[i]);
                }
                System.out.println("");                            
            }
        }
        catch ( NullPointerException ex ) {
            System.out.println("parameter is null: " + ex.getMessage());
        }
    }

    public static int[] removeDuplicates(int[] data) {
        try {
            int[] result = null;
            int count = 0;
            boolean dup;

            if ( data.length == 0 ) {
                return null;
            } else  if ( data.length == 1 ) {
                return data;
            } else {
                for (int i = 0; i < data.length; i++ ) {
                    dup = false;
                    for ( int j = (i+1); j < data.length; j++ ) {
                        if ( data[i] == data[j] ) {
                            dup = true;
                            break;
                        }
                    }
                    if ( dup == false ) {
                        result[count] = data[i];
                        count++;
                    }
                }            
            }
        }        
        catch ( NullPointerException ex ) {
            System.out.println("parameter is null: " + ex.getMessage());
        }

        return result;
    }

    
    public static int[] findDuplicates(int[] a1, int[] a2) {
        int result[] = null;
        int count = 0;
        for ( int i = 0; i < a1.length; i++ ) {
            for ( int j = 0; j < a2.length; j++ ) {
                if ( a1[i] == a2[j] ) {
                   result[count] = a1[i];
                   count++;
                }
            }
        }        
        return result;
    }

        public static void inputArray2d(int[][] data2d) {
        try {
            System.out.print("Please input the array's width and height : ");
            int width = Input.nextInt();
            if ( width < 0 ) {
               System.out.println("The array2d width should be an interger greater than 0");
               return;
            } 
            int height = Input.nextInt();
            if ( height < 0 ) {
               System.out.println("The array2d height should be an interger greater than 0");
               return;
            }            

            data2d = new int[width][height];
         
            for ( int i = 0; i < width; i++ ) {
                for ( int j = 0; j < height; j++ ) {
                    System.out.printf("Enter value of array[%d][%d] : ", i,j);
                    data2d[i][j] = Input.nextInt();
                }
            }    
        }
        catch ( InputMismatchException ex ) {
            System.out.println("You should input a interger value: " + ex.getMessage());
        }
    }

    public static void printArray2d(int[][] data2d) {
        System.out.println("Array data is : ");
        for ( int row = 0; row < data2d.length; row++ ) {
            for ( int col = 0; col < data2d[row].length; col++ ) {
                System.out.printf("%s%d", col == 0 ? "" : ", ", data2d[row][col]);
            }
            System.out.println("");
        }
        System.out.println("");
    }

    
    public static int[][] findDuplicates2d(int[][] a1, int[][] a2) {
        return null;
    }    
    
    public static int[] join(int[] a1, int []a2) {
        return null;
    } 
    
    public static void main(String[] args) {
        // TODO code application logic here
       
        int data[] = inputArray();
        printArray(data);
        
        
        int[] re = removeDuplicates(data);
        printArray(re);
        /*
        inputArray2d(data2d);
        printArray2d(data2d);
        */
        
    }
    
}
